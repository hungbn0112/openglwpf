﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace OpenGLWPF
{
    public class AppConfig : IAppConfig
    {
        public string ImageDir { get; set; }

        private AppConfig()
        {
            ImageDir = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "Images");
        }

        private static AppConfig _instance = new AppConfig();
        public static AppConfig Instance => _instance;
    }
}
