﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenGLWPF
{
    public interface IAppConfig
    {
        string ImageDir { get; }
    }
}
