﻿using OpenGL;
using OpenGLWPF.RenderUnit;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Linq;

namespace OpenGLWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        IEnumerable<IRenderUnit> _units;
        public MainWindow(IEnumerable<IRenderUnit> units)
        {
            InitializeComponent();
            _units = units;

            UnitList.ItemsSource = _units;
        }

        EventHandler<GlControlEventArgs> lastRender = null;
        EventHandler<GlControlEventArgs> contextUpdate = null;
        IRenderUnit _selectedUnit = null;
        private void RenderUnit_Selected(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            _selectedUnit = (IRenderUnit)e.AddedItems[0];
            //glControl.ContextCreated += (control, a) => selectedUnit.OnConfig(control as GlControl, a);
            glControl.Render -= lastRender;
            lastRender = (control, a) => _selectedUnit.OnRender(control as GlControl, a);
            glControl.Render += lastRender;
            glControl.ContextUpdate -= contextUpdate;
            contextUpdate = (control, a) => _selectedUnit.OnContextUpdate(control as GlControl, a);
            glControl.ContextUpdate += contextUpdate;
            _selectedUnit.OnConfig(glControl);
            glControl.Refresh();
        }

        private void KeyDown_Handler(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (_selectedUnit == null)
                return;

            _selectedUnit.OnInput(e);
        }
        bool mouseDown = false;
        private void MouseLeftButtonDown_Handled(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (_selectedUnit == null)
                return;
            mouseDown = true;
            _selectedUnit.OnMouse(e);
        }

        private void MouseLeftButtonHover_Handled(object sender, EventArgs e)
        {
           
        }

        private void MouseLeftButtonUp_Handled(object sender, EventArgs e)
        {
            mouseDown = false;
        }

        private void MouseLeftButtonMove_Handled(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (mouseDown)
            {
                if (_selectedUnit == null)
                    return;
                _selectedUnit.OnMouse(e);
            }
        }

        private void MouseScroll_Handled(object sender, System.Windows.Forms.ScrollEventArgs e)
        {
            if (_selectedUnit == null)
                return;
            
            _selectedUnit.OnScroll(e);
        }
    }
}
