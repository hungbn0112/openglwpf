﻿using OpenGL;
using System;
using System.Text;

namespace OpenGLWPF.Models
{
    class GLProgram : IDisposable
    {
        public GLProgram(string[] vertexSource, string[] fragmentSource)
        {
            // Create vertex and frament shaders
            // Note: they can be disposed after linking to program; resources are freed when deleting the program
            using (var vObject = new GLShaderObject(ShaderType.VertexShader, vertexSource))
            using (var fObject = new GLShaderObject(ShaderType.FragmentShader, fragmentSource))
            {
                // Create program
                ProgramName = Gl.CreateProgram();
                // Attach shaders
                Gl.AttachShader(ProgramName, vObject.ShaderName);
                Gl.AttachShader(ProgramName, fObject.ShaderName);
                // Link program
                Gl.LinkProgram(ProgramName);

                // Check linkage status
                int linked;

                Gl.GetProgram(ProgramName, ProgramProperty.LinkStatus, out linked);

                if (linked == 0)
                {
                    const int logMaxLength = 1024;

                    StringBuilder infolog = new StringBuilder(logMaxLength);
                    int infologLength;

                    Gl.GetProgramInfoLog(ProgramName, 1024, out infologLength, infolog);

                    throw new InvalidOperationException($"unable to link program: {infolog}");
                }
            }
        }

        public readonly uint ProgramName;

        public void Dispose()
        {
            Gl.DeleteProgram(ProgramName);
        }

        public uint GetAttributeLocation(string attributeName)
        {
            var locationPosition = (uint)Gl.GetAttribLocation(ProgramName, attributeName);
            if (locationPosition < 0)
                throw new InvalidOperationException(String.Format("No attribute with name {0}", attributeName));
            return locationPosition;
        }

        public int GetUnitformLocation(string uniformName)
        {
            var uniformLocation = Gl.GetUniformLocation(ProgramName, uniformName);
            if (uniformLocation < 0)
                throw new InvalidOperationException(String.Format("No uniform with name {0}", uniformName));
            return uniformLocation;
        }
    }
}
