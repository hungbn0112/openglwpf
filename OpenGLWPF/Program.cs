﻿using OpenGLWPF.RenderUnit;
using SimpleInjector;
using System;
using System.Reflection;

namespace OpenGLWPF
{
    class Program
    {
        [STAThread]
        static void Main()
        {
            var container = Bootstrap();
            RunApplication(container);
        }

        private static Container Bootstrap()
        {
            // Create the container as usual.
            var container = new Container();

            //container.RegisterManyForOpenGeneric(typeof(IRenderUnit), Assembly.GetExecutingAssembly());
            // Register your types, for instance:
            //container.Register<IQueryProcessor, QueryProcessor>(Lifestyle.Singleton);
            //container.Register<IUserContext, WpfUserContext>();
            container.Collection.Register(typeof(IRenderUnit), new[] { Assembly.GetExecutingAssembly() });

            // Register your windows and view models:
            container.Register<MainWindow>();
            //container.Register<MainWindowViewModel>();
            container.RegisterInstance(typeof(IAppConfig), AppConfig.Instance);
            container.Verify();

            return container;
        }

        private static void RunApplication(Container container)
        {
            var app = new App();
            var mainWindow = container.GetInstance<MainWindow>();
            app.Run(mainWindow);
        }
    }
}
