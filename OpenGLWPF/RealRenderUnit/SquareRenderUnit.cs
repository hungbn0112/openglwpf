﻿using OpenGL;
using OpenGLWPF.Models;
using OpenGLWPF.RenderUnit;
using System;
using System.ComponentModel.Composition;

namespace OpenGLWPF.RealRenderUnit
{
    [Export(typeof(IRenderUnit))]
    public class SquareRenderUnit : BaseRenderUnit
    {
        public override string Name => "Square";
        uint[] vbos = new uint[2];
        uint[] vaos = new uint[1];

        private GLProgram Program;
        public int LocationMVP;
        public uint LocationPosition;
        public uint LocationColor;

        private static float _Angle = 0.0f;
        public float[] ProjectionModelMatrix;
        public override void OnConfig(GlControl control)
        {
            int vpw = control.ClientSize.Width;
            int vph = control.ClientSize.Height;

            Gl.Viewport(0, 0, vpw, vph);
            Gl.MatrixMode(MatrixMode.Projection);
            Gl.LoadIdentity();
            Gl.Ortho(0.0, 100.0, 0.0, 100.0, -1.0, 1.0);

            Gl.MatrixMode(MatrixMode.Modelview);
            Gl.LoadIdentity();
        }

        public override void OnRender(GlControl control, GlControlEventArgs a)
        {
            Gl.Clear(ClearBufferMask.ColorBufferBit);
            Gl.ClearColor(1.0f, 1.0f, 1.0f, 0.0f);

            Gl.Color3(0.0f, 0.0f, 0.0f);
            Gl.Begin(PrimitiveType.Polygon);
                Gl.Vertex3(20.0, 20.0, 0.0);
                Gl.Vertex3(80.0, 20.0, 0.0);
                Gl.Vertex3(80.0, 80.0, 0.0);
                Gl.Vertex3(20.0, 80.0, 0.0);
            Gl.End();
            Gl.Flush();
        }
    }
}
