﻿using OpenGL;
using OpenGLWPF.Models;
using OpenGLWPF.RenderUnit;
using System;
using System.ComponentModel.Composition;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace OpenGLWPF.RealRenderUnit
{
    [Export(typeof(IRenderUnit))]
    public class TriangleCameraRotateRenderUnit : BaseRenderUnit
    {
        public override string Name => "Camera Rotate";
        uint[] vbos = new uint[1];
        uint[] vaos = new uint[1];
        uint texture;

        private GLProgram Program;
        public int LocationMVP;
        public uint LocationPosition;
        public uint LocationColor;
        public uint LocationTexture;

        private static float _Angle = 0f;
        public float[] ProjectionModelMatrix;

        private IAppConfig _appConfig;
        public TriangleCameraRotateRenderUnit(IAppConfig appConfig)
        {
            _appConfig = appConfig;
        }

        public override void OnContextUpdate(GlControl control, GlControlEventArgs a)
        {
            _Angle = (_Angle + 0.05f) % 360.0f;
        }

        public override void OnConfig(GlControl control)
        {
            Program = new GLProgram(_VertexSourceGL, _FragmentSourceGL);

            var bitmap = Bitmap.FromFile(Path.Combine(_appConfig.ImageDir, "container.jpg")) as Bitmap;

            Gl.UseProgram(Program.ProgramName);
            
            Gl.Enable(EnableCap.DepthTest);

            LocationMVP = Program.GetUnitformLocation("uMVP");
            LocationPosition = Program.GetAttributeLocation("aPosition");
            LocationTexture = Program.GetAttributeLocation("aTexture");

            Gl.GenVertexArrays(vaos);
            Gl.BindVertexArray(vaos[0]);

            Gl.GenBuffers(vbos);

            texture = Gl.GenTexture();

            BitmapData iBitmapData = null;
            using (MemoryLock vertexArrayLock = new MemoryLock(_ArrayPosition))
            //using (MemoryLock textureLock = new MemoryLock(imageArr))
            {
                Gl.BindBuffer(BufferTarget.ArrayBuffer, vbos[0]);
                Gl.BufferData(BufferTarget.ArrayBuffer, (uint)_ArrayPosition.Length * sizeof(float), vertexArrayLock.Address, BufferUsage.StaticDraw);

                Gl.VertexAttribPointer(LocationPosition, 3, VertexAttribType.Float, false, 5 * sizeof(float), IntPtr.Zero);
                Gl.EnableVertexAttribArray(LocationPosition);
                Gl.VertexAttribPointer(LocationTexture, 2, VertexAttribType.Float, false, 5 * sizeof(float), new IntPtr(3 * sizeof(float)));
                Gl.EnableVertexAttribArray(LocationTexture);

                Gl.ActiveTexture(TextureUnit.Texture0); // activate the texture unit first before binding texture
                Gl.BindTexture(TextureTarget.Texture2d, texture);
                Gl.TexParameter(TextureTarget.Texture2d, TextureParameterName.TextureWrapS, Gl.REPEAT);
                Gl.TexParameter(TextureTarget.Texture2d, TextureParameterName.TextureWrapT, Gl.REPEAT);
                Gl.TexParameter(TextureTarget.Texture2d, TextureParameterName.TextureMinFilter, Gl.LINEAR);
                Gl.TexParameter(TextureTarget.Texture2d, TextureParameterName.TextureMagFilter, Gl.LINEAR);

                try
                {
                    iBitmapData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), System.Drawing.Imaging.ImageLockMode.ReadOnly, bitmap.PixelFormat);

                    Gl.TexImage2D(TextureTarget.Texture2d, 0, InternalFormat.Rgb, bitmap.Width, bitmap.Height, 0, OpenGL.PixelFormat.Bgr, PixelType.UnsignedByte, iBitmapData.Scan0);
                }
                finally
                {
                    if (iBitmapData != null)
                        bitmap.UnlockBits(iBitmapData);
                }
                
                Gl.GenerateMipmap(TextureTarget.Texture2d);
            }

            Gl.BindVertexArray(0);
            Gl.BindBuffer(BufferTarget.ArrayBuffer, 0);
            Gl.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
        }

        float _radius = 3.0f;
        public override void OnRender(GlControl control, GlControlEventArgs a)
        {
            int vpw = control.ClientSize.Width;
            int vph = control.ClientSize.Height;

            Gl.Viewport(0, 0, vpw, vph);
            Gl.UseProgram(Program.ProgramName);

            Gl.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            Gl.ClearColor(0.2f, 0.3f, 0.3f, 1.0f);

            Gl.BindVertexArray(vaos[0]);

            var projection = new PerspectiveProjectionMatrix(45.0f, (float)control.ClientSize.Width / (float)control.ClientSize.Height, 1.0f, 100.0f);
            var modelview = new ModelMatrix();
            modelview.LookAtTarget(
                new Vertex3f((float)Math.Sin(_Angle) * _radius, 0.0f, (float)Math.Cos(_Angle) * _radius),
                Vertex3f.Zero,
                Vertex3f.UnitY
            );
            ProjectionModelMatrix = (projection * modelview).ToArray();
            Gl.UniformMatrix4(LocationMVP, false, ProjectionModelMatrix);
            
            Gl.DrawArrays(PrimitiveType.Triangles, 0, 36);
            
            Gl.BindVertexArray(0);
        }

        /// <summary>
        /// Vertex position array.
        /// // first triangle
        /// </summary>
        private static readonly float[] _ArrayPosition = new float[] {
            -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
             0.5f, -0.5f, -0.5f,  1.0f, 0.0f,
             0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
             0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
            -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
            -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,

            -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
             0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
             0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
             0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
            -0.5f,  0.5f,  0.5f,  0.0f, 1.0f,
            -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,

            -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
            -0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
            -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
            -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
            -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
            -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

             0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
             0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
             0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
             0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
             0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
             0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

            -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
             0.5f, -0.5f, -0.5f,  1.0f, 1.0f,
             0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
             0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
            -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
            -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,

            -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
             0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
             0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
             0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
            -0.5f,  0.5f,  0.5f,  0.0f, 0.0f,
            -0.5f,  0.5f, -0.5f,  0.0f, 1.0f
        };

        private static readonly float[] cubePositions = new float[] {
           0.0f,  0.0f,  0.0f,
           2.0f,  5.0f, -15.0f,
          -1.5f, -2.2f, -2.5f,
          -3.8f, -2.0f, -12.3f,
           2.4f, -0.4f, -3.5f,
          -1.7f,  3.0f, -7.5f,
           1.3f, -2.0f, -2.5f,
           1.5f,  2.0f, -2.5f,
           1.5f,  0.2f, -1.5f,
          -1.3f,  1.0f, -1.5f
        };

        private readonly string[] _VertexSourceGL = {
            "uniform mat4 uMVP;\n",
            "attribute vec3 aPosition;\n",
            "attribute vec2 aTexture;\n",
            "varying vec2 TexCoord;\n",
            "void main() {\n",
            "	gl_Position = uMVP * vec4(aPosition, 1.0);\n",
            "	TexCoord = aTexture;\n",
            "}\n"
        };

        private readonly string[] _FragmentSourceGL = {
            "precision mediump float;\n",
            "varying vec2 TexCoord;\n",
            "uniform sampler2D ourTexture;\n",
            "void main() {\n",
            "	gl_FragColor = texture(ourTexture, TexCoord);\n",
            "}\n"
        };
    }
}
