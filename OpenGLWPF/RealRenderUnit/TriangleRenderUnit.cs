﻿using OpenGL;
using OpenGLWPF.RenderUnit;
using System.ComponentModel.Composition;

namespace OpenGLWPF.RealRenderUnit
{
    [Export(typeof(IRenderUnit))]
    public class TriangleRenderUnit : BaseRenderUnit
    {
        public override void OnConfig(GlControl control)
        {
            Gl.MatrixMode(MatrixMode.Projection);
            Gl.LoadIdentity();
            //Gl.Ortho(0.0, 1.0f, 0.0, 1.0, 0.0, 1.0);

            Gl.MatrixMode(MatrixMode.Modelview);
            Gl.LoadIdentity();
        }

        public override void OnRender(GlControl control, GlControlEventArgs a)
        {
            int vpx = 0;
            int vpy = 0;
            int vpw = control.ClientSize.Width;
            int vph = control.ClientSize.Height;

            Gl.Viewport(vpx, vpy, vpw, vph);
            Gl.Clear(ClearBufferMask.ColorBufferBit);

            // Old school OpenGL 1.1
            // Setup & enable client states to specify vertex arrays, and use Gl.DrawArrays instead of Gl.Begin/End paradigm
            using (MemoryLock vertexArrayLock = new MemoryLock(_ArrayPosition))
            using (MemoryLock vertexColorLock = new MemoryLock(_ArrayColor))
            {
                // Note: the use of MemoryLock objects is necessary to pin vertex arrays since they can be reallocated by GC
                // at any time between the Gl.VertexPointer execution and the Gl.DrawArrays execution

                Gl.VertexPointer(2, VertexPointerType.Float, 0, vertexArrayLock.Address);
                Gl.EnableClientState(EnableCap.VertexArray);

                Gl.ColorPointer(3, ColorPointerType.Float, 0, vertexColorLock.Address);
                Gl.EnableClientState(EnableCap.ColorArray);

                Gl.DrawArrays(PrimitiveType.Triangles, 0, 3);
            }
        }

        /// <summary>
        /// Vertex position array.
        /// </summary>
        private static readonly float[] _ArrayPosition = new float[] {
            0.0f, 0.5f,
            -0.5f, -0.5f,
            0.5f, -0.5f
        };

        /// <summary>
        /// Vertex color array.
        /// </summary>
        private static readonly float[] _ArrayColor = new float[] {
            1.0f, 0.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 0.0f, 1.0f
        };

        public override string Name => "Triangle";
    }
}
