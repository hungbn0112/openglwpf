﻿using OpenGL;
using OpenGLWPF.Models;
using OpenGLWPF.RenderUnit;
using System;
using System.ComponentModel.Composition;

namespace OpenGLWPF.RealRenderUnit
{
    [Export(typeof(IRenderUnit))]
    public class TriangleVBOElementRenderUnit : BaseRenderUnit
    {
        public override string Name => "Triangle VBO DrawElement";
        uint[] vbos = new uint[3];
        uint[] vaos = new uint[1];

        private GLProgram Program;
        public int LocationMVP;
        public uint LocationPosition;
        public uint LocationColor;

        private static float _Angle = 0.0f;
        public float[] ProjectionModelMatrix;
        public override void OnConfig(GlControl control)
        {
            Program = new GLProgram(_VertexSourceGL, _FragmentSourceGL);

            Gl.UseProgram(Program.ProgramName);

            LocationMVP = Program.GetUnitformLocation("uMVP");
            LocationPosition = Program.GetAttributeLocation("aPosition");
            LocationColor = Program.GetAttributeLocation("aColor");

            var projection = new OrthoProjectionMatrix(-1.0f, +1.0f, -1.0f, +1.0f);
            var modelview = new ModelMatrix();
            //modelview.Translate(-0.5f, -0.5f, 0.0f);
            modelview.RotateZ(_Angle);
            ProjectionModelMatrix = (projection * modelview).ToArray();

            Gl.GenVertexArrays(vaos);
            Gl.BindVertexArray(vaos[0]);

            Gl.GenBuffers(vbos);

            using (MemoryLock vertexArrayLock = new MemoryLock(_ArrayPosition))
            using (MemoryLock vertexColorLock = new MemoryLock(_ArrayColor))
            using (MemoryLock vertexIndexLock = new MemoryLock(_ArrayIndices))
            {
                Gl.BindBuffer(BufferTarget.ArrayBuffer, vbos[0]);
                Gl.BufferData(BufferTarget.ArrayBuffer, (uint)_ArrayPosition.Length * sizeof(float), vertexArrayLock.Address, BufferUsage.StaticDraw);

                Gl.VertexAttribPointer(LocationPosition, 3, VertexAttribType.Float, false, 0, IntPtr.Zero);
                Gl.EnableVertexAttribArray(LocationPosition);

                Gl.BindBuffer(BufferTarget.ElementArrayBuffer, vbos[1]);
                Gl.BufferData(BufferTarget.ElementArrayBuffer, (uint)_ArrayIndices.Length * sizeof(ushort), vertexIndexLock.Address, BufferUsage.StaticDraw);

                Gl.BindBuffer(BufferTarget.ArrayBuffer, vbos[2]);
                Gl.BufferData(BufferTarget.ArrayBuffer, (uint)_ArrayColor.Length * sizeof(float), vertexColorLock.Address, BufferUsage.StaticDraw);
                Gl.VertexAttribPointer(LocationColor, 3, VertexAttribType.Float, false, 0, IntPtr.Zero);
                Gl.EnableVertexAttribArray(LocationColor);
            }

            Gl.BindVertexArray(0);
            Gl.BindBuffer(BufferTarget.ArrayBuffer, 0);
            Gl.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
        }

        public override void OnRender(GlControl control, GlControlEventArgs a)
        {
            int vpw = control.ClientSize.Width;
            int vph = control.ClientSize.Height;

            Gl.Viewport(0, 0, vpw, vph);
            Gl.Clear(ClearBufferMask.ColorBufferBit);

            Gl.UseProgram(Program.ProgramName);
            Gl.BindVertexArray(vaos[0]);
            
            Gl.UniformMatrix4(LocationMVP, false, ProjectionModelMatrix);
            
            Gl.DrawElements(PrimitiveType.Triangles, 6, DrawElementsType.UnsignedShort, IntPtr.Zero);
            
            Gl.BindVertexArray(0);
        }

        /// <summary>
        /// Vertex position array.
        /// // first triangle
        // 0.5f,  0.5f, 0.0f,  // top right
        // 0.5f, -0.5f, 0.0f,  // bottom right
        //-0.5f,  0.5f, 0.0f,  // top left 
        //// second triangle
        // 0.5f, -0.5f, 0.0f,  // bottom right
        //-0.5f, -0.5f, 0.0f,  // bottom left
        //-0.5f,  0.5f, 0.0f   // top left
        /// </summary>
        private static readonly float[] _ArrayPosition = new float[] {
             0.5f,  0.5f, 0.0f,  // top right
             0.5f, -0.5f, 0.0f,  // bottom right
            -0.5f, -0.5f, 0.0f,  // bottom left
            -0.5f,  0.5f, 0.0f   // top left 
        };

       private static readonly ushort[] _ArrayIndices = new ushort[] {  // note that we start from 0!
            0, 1, 3,   // first triangle
            1, 2, 3    // second triangle
        };

        /// <summary>
        /// Vertex color array.
        /// </summary>
        private static readonly float[] _ArrayColor = new float[] {
            1.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 0.0f, 1.0f
        };

        private static readonly float[] _ArrayTexCoords = new float[] {
            0.0f, 0.0f,  // lower-left corner  
            1.0f, 0.0f,  // lower-right corner
            0.5f, 1.0f   // top-center corner
        };

        private readonly string[] _VertexSourceGL = {
            "uniform mat4 uMVP;\n",
            "attribute vec2 aPosition;\n",
            "attribute vec3 aColor;\n",
            "varying vec3 vColor;\n",
            "void main() {\n",
            "	gl_Position = uMVP * vec4(aPosition, 0.0, 1.0);\n",
            "	vColor = aColor;\n",
            "}\n"
        };

        private readonly string[] _FragmentSourceGL = {
            "precision mediump float;\n",
            "void main() {\n",
            "	gl_FragColor = vec4(1.0, 1.0, 0.0, 1.0);\n",
            "}\n"
        };
    }
}
