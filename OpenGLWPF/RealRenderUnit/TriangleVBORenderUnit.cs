﻿using OpenGL;
using OpenGLWPF.Models;
using OpenGLWPF.RenderUnit;
using System;
using System.ComponentModel.Composition;

namespace OpenGLWPF.RealRenderUnit
{
    [Export(typeof(IRenderUnit))]
    public class TriangleVBORenderUnit : BaseRenderUnit
    {
        public override string Name => "Triangle VBO";
        uint[] vbos = new uint[2];
        uint[] vaos = new uint[1];

        private GLProgram Program;
        public int LocationMVP;
        public uint LocationPosition;
        public uint LocationColor;

        private static float _Angle = 0.0f;
        public float[] ProjectionModelMatrix;
        public override void OnConfig(GlControl control)
        {
            Program = new GLProgram(_VertexSourceGL, _FragmentSourceGL);

            Gl.UseProgram(Program.ProgramName);

            // Get uniform locations
            if ((LocationMVP = (int)Gl.GetUniformLocation(Program.ProgramName, "uMVP")) < 0)
                throw new InvalidOperationException("no uniform uMVP");
            // Get attributes locations
            if ((LocationPosition = (uint)Gl.GetAttribLocation(Program.ProgramName, "aPosition")) < 0)
                throw new InvalidOperationException("no attribute aPosition");
            if ((LocationColor = (uint)Gl.GetAttribLocation(Program.ProgramName, "aColor")) < 0)
                throw new InvalidOperationException("no attribute aColor");

            var projection = new OrthoProjectionMatrix(-1.0f, +1.0f, -1.0f, +1.0f);
            var modelview = new ModelMatrix();
            //modelview.Translate(-0.5f, -0.5f, 0.0f);
            modelview.RotateZ(_Angle);
            ProjectionModelMatrix = (projection * modelview).ToArray();

            Gl.GenVertexArrays(vaos);
            Gl.BindVertexArray(vaos[0]);

            Gl.GenBuffers(vbos);

            using (MemoryLock vertexArrayLock = new MemoryLock(_ArrayPosition))
            using (MemoryLock vertexColorLock = new MemoryLock(_ArrayColor))
            {
                Gl.BindBuffer(BufferTarget.ArrayBuffer, vbos[0]);
                Gl.BufferData(BufferTarget.ArrayBuffer, (uint)_ArrayPosition.Length * sizeof(float), vertexArrayLock.Address, BufferUsage.StaticDraw);
                Gl.EnableVertexAttribArray(LocationPosition);
                Gl.VertexAttribPointer(LocationPosition, 2, VertexAttribType.Float, false, 0, IntPtr.Zero);

                Gl.BindBuffer(BufferTarget.ArrayBuffer, vbos[1]);
                Gl.BufferData(BufferTarget.ArrayBuffer, (uint)_ArrayColor.Length * sizeof(float), vertexColorLock.Address, BufferUsage.StaticDraw);
                Gl.EnableVertexAttribArray(LocationColor);
                Gl.VertexAttribPointer(LocationColor, 3, VertexAttribType.Float, false, 0, IntPtr.Zero);
            }

            Gl.BindBuffer(BufferTarget.ArrayBuffer, 0);
            Gl.BindVertexArray(0);
        }

        public override void OnRender(GlControl control, GlControlEventArgs a)
        {
            int vpw = control.ClientSize.Width;
            int vph = control.ClientSize.Height;

            Gl.Viewport(0, 0, vpw, vph);
            Gl.Clear(ClearBufferMask.ColorBufferBit);

            Gl.UseProgram(Program.ProgramName);
            Gl.BindVertexArray(vaos[0]);

            Gl.UniformMatrix4(LocationMVP, false, ProjectionModelMatrix);

            Gl.DrawArrays(PrimitiveType.Triangles, 0, 3);

            Gl.BindVertexArray(0);
        }

        /// <summary>
        /// Vertex position array.
        /// </summary>
        private static readonly float[] _ArrayPosition = new float[] {
            0.0f, 0.5f,
            -0.5f, -0.5f,
            0.5f, -0.5f
        };

        /// <summary>
        /// Vertex color array.
        /// </summary>
        private static readonly float[] _ArrayColor = new float[] {
            1.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 0.0f, 1.0f
        };

        private readonly string[] _VertexSourceGL = {
            "uniform mat4 uMVP;\n",
            "attribute vec2 aPosition;\n",
            "attribute vec3 aColor;\n",
            "varying vec3 vColor;\n",
            "void main() {\n",
            "	gl_Position = uMVP * vec4(aPosition, 0.0, 1.0);\n",
            "	vColor = aColor;\n",
            "}\n"
        };

        private readonly string[] _FragmentSourceGL = {
            "precision mediump float;\n",
            "varying vec3 vColor;\n",
            "void main() {\n",
            "	gl_FragColor = vec4(vColor, 1.0);\n",
            "}\n"
        };
    }
}
