﻿using OpenGL;
using OpenGLWPF.Models;
using OpenGLWPF.RenderUnit;
using System;
using System.ComponentModel.Composition;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace OpenGLWPF.RealRenderUnit
{
    [Export(typeof(IRenderUnit))]
    public class TriangleVBOTextureRenderUnit : BaseRenderUnit
    {
        public override string Name => "Triangle VBO Texture";
        uint[] vbos = new uint[2];
        uint[] vaos = new uint[1];
        uint texture;

        private GLProgram Program;
        public int LocationMVP;
        public uint LocationPosition;
        public uint LocationColor;
        public uint LocationTexture;

        private static float _Angle = 0f;
        public float[] ProjectionModelMatrix;

        private IAppConfig _appConfig;
        public TriangleVBOTextureRenderUnit(IAppConfig appConfig)
        {
            _appConfig = appConfig;
        }

        public override void OnConfig(GlControl control)
        {
            Program = new GLProgram(_VertexSourceGL, _FragmentSourceGL);

            var bitmap = Bitmap.FromFile(Path.Combine(_appConfig.ImageDir, "container.jpg")) as Bitmap;
            //Bitmap bitmap = new Bitmap(image.Width, image.Height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            //for (int i = 0; i < image.Width; i++)
            //{
            //    for (int j = 0; j < image.Height; j++)
            //    {
            //        Color temp = image.GetPixel(i, j);
            //        bitmap.SetPixel(i, j, temp);
            //    }
            //}
            //byte[] imageArr;
            //using (MemoryStream ms = new MemoryStream())
            //{
            //    bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
            //    imageArr = ms.ToArray();
            //}

            Gl.UseProgram(Program.ProgramName);

            LocationMVP = Program.GetUnitformLocation("uMVP");
            LocationPosition = Program.GetAttributeLocation("aPosition");
            LocationColor = Program.GetAttributeLocation("aColor");
            LocationTexture = Program.GetAttributeLocation("aTexture");

            var projection = new OrthoProjectionMatrix(-1.0f, +1.0f, -1.0f, +1.0f, 0.0f, 50.0f);
            var modelview = new ModelMatrix();
            modelview.SetIdentity();
            modelview.Translate(0f, 0.0f, 0.0f);
            modelview.RotateZ(_Angle);
            ProjectionModelMatrix = (projection * modelview).ToArray();

            Gl.GenVertexArrays(vaos);
            Gl.BindVertexArray(vaos[0]);

            Gl.GenBuffers(vbos);

            texture = Gl.GenTexture();

            BitmapData iBitmapData = null;
            using (MemoryLock vertexArrayLock = new MemoryLock(_ArrayPosition))
            using (MemoryLock vertexIndexLock = new MemoryLock(_ArrayIndices))
            //using (MemoryLock textureLock = new MemoryLock(imageArr))
            {
                Gl.BindBuffer(BufferTarget.ArrayBuffer, vbos[0]);
                Gl.BufferData(BufferTarget.ArrayBuffer, (uint)_ArrayPosition.Length * sizeof(float), vertexArrayLock.Address, BufferUsage.StaticDraw);

                Gl.VertexAttribPointer(LocationPosition, 3, VertexAttribType.Float, false, 8 * sizeof(float), IntPtr.Zero);
                Gl.EnableVertexAttribArray(LocationPosition);
                Gl.VertexAttribPointer(LocationColor, 3, VertexAttribType.Float, false, 8 * sizeof(float), new IntPtr(3 * sizeof(float)));
                Gl.EnableVertexAttribArray(LocationColor);
                Gl.VertexAttribPointer(LocationTexture, 2, VertexAttribType.Float, false, 8 * sizeof(float), new IntPtr(6 * sizeof(float)));
                Gl.EnableVertexAttribArray(LocationTexture);

                Gl.BindBuffer(BufferTarget.ElementArrayBuffer, vbos[1]);
                Gl.BufferData(BufferTarget.ElementArrayBuffer, (uint)_ArrayIndices.Length * sizeof(uint), vertexIndexLock.Address, BufferUsage.StaticDraw);

                Gl.ActiveTexture(TextureUnit.Texture0); // activate the texture unit first before binding texture
                Gl.BindTexture(TextureTarget.Texture2d, texture);
                Gl.TexParameter(TextureTarget.Texture2d, TextureParameterName.TextureWrapS, Gl.REPEAT);
                Gl.TexParameter(TextureTarget.Texture2d, TextureParameterName.TextureWrapT, Gl.REPEAT);
                Gl.TexParameter(TextureTarget.Texture2d, TextureParameterName.TextureMinFilter, Gl.LINEAR);
                Gl.TexParameter(TextureTarget.Texture2d, TextureParameterName.TextureMagFilter, Gl.LINEAR);

                try
                {
                    iBitmapData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), System.Drawing.Imaging.ImageLockMode.ReadOnly, bitmap.PixelFormat);

                    Gl.TexImage2D(TextureTarget.Texture2d, 0, InternalFormat.Rgb, bitmap.Width, bitmap.Height, 0, OpenGL.PixelFormat.Bgr, PixelType.UnsignedByte, iBitmapData.Scan0);
                }
                finally
                {
                    if (iBitmapData != null)
                        bitmap.UnlockBits(iBitmapData);
                }
                //Gl.TexImage2D(TextureTarget.Texture2d, 0, InternalFormat.Rgb, image.Width, image.Height, 0, PixelFormat.Rgb, PixelType.Byte, textureLock.Address);
                Gl.GenerateMipmap(TextureTarget.Texture2d);
            }

            Gl.BindVertexArray(0);
            Gl.BindBuffer(BufferTarget.ArrayBuffer, 0);
            Gl.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
        }

        public override void OnRender(GlControl control, GlControlEventArgs a)
        {
            int vpw = control.ClientSize.Width;
            int vph = control.ClientSize.Height;

            Gl.Viewport(0, 0, vpw, vph);
            Gl.Clear(ClearBufferMask.ColorBufferBit);

            Gl.UseProgram(Program.ProgramName);
            Gl.BindVertexArray(vaos[0]);
            
            Gl.UniformMatrix4(LocationMVP, false, ProjectionModelMatrix);
            
            Gl.DrawElements(PrimitiveType.Triangles, 6, DrawElementsType.UnsignedInt, IntPtr.Zero);
            
            Gl.BindVertexArray(0);
        }

        /// <summary>
        /// Vertex position array.
        /// // first triangle
        /// </summary>
        private static readonly float[] _ArrayPosition = new float[] {
            // positions          // colors           // texture coords
             0.5f,  0.5f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 1.0f,   // top right
             0.5f, -0.5f, 0.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f,   // bottom right
            -0.5f, -0.5f, 0.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f,   // bottom left
            -0.5f,  0.5f, 0.0f,   1.0f, 1.0f, 0.0f,   0.0f, 1.0f    // top left 
        };

        private static readonly uint[] _ArrayIndices = new uint[] {  // note that we start from 0!
            0, 1, 3,   // first triangle
            1, 2, 3    // second triangle
        };

        private readonly string[] _VertexSourceGL = {
            "uniform mat4 uMVP;\n",
            "attribute vec3 aPosition;\n",
            "attribute vec3 aColor;\n",
            "attribute vec2 aTexture;\n",
            "varying vec3 vColor;\n",
            "varying vec2 TexCoord;\n",
            "void main() {\n",
            "	gl_Position = uMVP * vec4(aPosition, 1.0);\n",
            "	vColor = aColor;\n",
            "	TexCoord = aTexture;\n",
            "}\n"
        };

        private readonly string[] _FragmentSourceGL = {
            "precision mediump float;\n",
            "varying vec2 TexCoord;\n",
            "varying vec3 vColor;\n",
            "uniform sampler2D ourTexture;\n",
            "void main() {\n",
            "	gl_FragColor = texture(ourTexture, TexCoord);\n",
            "}\n"
        };
    }
}
