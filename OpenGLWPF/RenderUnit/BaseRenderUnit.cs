﻿using System.Windows;
using System.Windows.Input;
using OpenGL;

namespace OpenGLWPF.RenderUnit
{
    public class BaseRenderUnit : IRenderUnit
    {
        public virtual string Name => "";

        public virtual void OnConfig(GlControl control)
        {
            
        }

        public virtual void OnContextUpdate(GlControl control, GlControlEventArgs a)
        {
            
        }

        public virtual void OnInput(KeyEventArgs keyEvent)
        {
            
        }

        public virtual void OnMouse(System.Windows.Forms.MouseEventArgs mouseEvent)
        {
            
        }

        public virtual void OnRender(GlControl control, GlControlEventArgs a)
        {
            
        }

        public virtual void OnScroll(System.Windows.Forms.ScrollEventArgs scrollEvent)
        {
            
        }
    }
}
