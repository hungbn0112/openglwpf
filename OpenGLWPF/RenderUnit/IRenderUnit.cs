﻿using OpenGL;
using System.Windows;
using System.Windows.Input;

namespace OpenGLWPF.RenderUnit
{
    public interface IRenderUnit
    {
        string Name { get; }
        void OnConfig(GlControl control);
        void OnRender(GlControl control, GlControlEventArgs a);
        void OnContextUpdate(GlControl control, GlControlEventArgs a);
        void OnInput(KeyEventArgs keyEvent);
        void OnMouse(System.Windows.Forms.MouseEventArgs mouseEvent);
        void OnScroll(System.Windows.Forms.ScrollEventArgs scrollEvent);
    }
}